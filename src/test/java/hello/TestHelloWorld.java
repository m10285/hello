package hello;
import static org.junit.Assert.*;

import hello.HelloWorld;

import org.junit.Test;

public class TestHelloWorld {

  @Test
  public void testSayHi() {
    HelloWorld h = new HelloWorld();
    String result = h.sayHi();
    assertEquals("Hello World!", result);
  }
  
}
